# src/ccd_map/console.py
import click
from . import __version__


@click.command()
@click.version_option(version=__version__)
def main():
    """ccd_proc test."""
    click.echo("Hello, world!")
