Level 2 Data
============

ESDT product name: TOMSN7L2

Data storage locations
----------------------


| System |                        Location| 
|---|---|
| GES DISC |                    https://acdisc.gesdisc.eosdis.nasa.gov/data/Nimbus7_TOMS_Level2/TOMSN7L2 |
|---|----|




 614 Cluster                  /science/toms/production/data/n7/L2HDF
 TLCF                         /ozone/toms/n7/L2HDF
 TLCF (mirror of GES DISC)    /tis/acps/GES_DISC/008/TOMSN7L2


[Webpage](https://disc.gsfc.nasa.gov/datasets/TOMSN7L2_008/summary) for TOMSN7L2 at the GES DISC.

== Software:

TOMSN7L2 0.9.30
file:///omi/cm/svn/app/TOMSN7L2/tags/0.9.30
